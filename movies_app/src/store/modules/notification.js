import mutations from "../mutations";

const { SHOW_TOAST_NOTIFY, SHOW_NOTIFY } = mutations;

const notificationStore = {
  state: {
    massgePool: [],
    showNotifyTemplate: false,
  },
  getters: {
    massgePool: ({ massgePool }) => massgePool[massgePool.length - 1],
    showNotifyTemplate: ({ showNotifyTemplate }) => showNotifyTemplate,
  },
  mutations: {
    [SHOW_TOAST_NOTIFY](state, msg) {
      state.massgePool.push(msg);
    },
    [SHOW_NOTIFY](state, bool) {
      state.showNotifyTemplate = bool;
    },
  },
  actions: {
    showNotify({ commit }, msg) {
      commit(SHOW_TOAST_NOTIFY, msg);
    },
    toggleShow({ commit }, bool) {
      commit(SHOW_NOTIFY, bool);
    },
  },
};

export default notificationStore;
